import { Component, OnInit } from '@angular/core';
import { RestCommService } from './service/rest-comm.service'
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  dataList: any;
  credForm = new FormGroup({ "id":new FormControl(''),"email": new FormControl(''), "password": new FormControl('') });;

  constructor(private restComm: RestCommService) { }
  getDataList() {
    this.restComm.getEmailList().subscribe((res) => {
      this.dataList = res.data;
      console.log(this.dataList);
    }, (err) => {
      alert(err.error.message);
    })
  }
  onSubmit() {

    let data =this.credForm.getRawValue();
    if(data.id){this.updateCreds(data)}else{
      this.saveCreds(data);
    }
  }
  editForm(data){
    this.credForm.patchValue(data);
  }
  saveCreds(FormData) {
    
    this.restComm.saveUserCred(FormData).subscribe((res) => {
       alert(res.message);
       this.credForm.reset();
       this.getDataList(); 
      }
      , (err) => {
        console.log(err.error.message);
        alert(err.error.message);
      });
  }

  updateCreds(FormData){
    this.restComm.updateUserCred(FormData).subscribe((res) => {
      alert(res.message);
      this.credForm.reset();
      this.getDataList(); 
     }
     , (err) => {
       console.log(err.error.message);
       this.credForm.reset();
       alert(err.error.message);
       
     });
  }
  deleteCreds(id){
    this.restComm.deleteCred(id).subscribe((res) => {
      alert(res.message);
      
      this.getDataList(); 
     }
     , (err) => {
       console.log(err.error.message);
       alert(err.error.message);
     });
  }
  ngOnInit() {
    this.getDataList();

  }
}
