exports.catchError=(error,req,res,next)=>{
    
let status=error.status||500;
    res.status(status).json({message:error.message});
}