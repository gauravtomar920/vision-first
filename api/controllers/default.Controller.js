
const nodemailer = require("nodeMailer");
const fs = require('fs');
const pth = require("path");
var url = require('url');

exports.constants = {
  dir: "files",
  fileName: "credList.json",
};

exports.home = (req, res, next) => {

  res.json({ "message": 'working' });

}
exports.readList = function () {

  let consts = this.constants;
  path = consts.dir + pth.sep + consts.fileName;
  let fileContentStr = '';
  let JSONData = {};
  try {
    if (fs.existsSync(path)) {
      fileContentStr = fs.readFileSync(path, { encoding: 'utf8', flag: 'r' });
      JSONData = JSON.parse(fileContentStr);

      if (typeof (JSONData === "object")) {


        return JSONData;
      } else {
        throw new Error("Corrupted JSON");
      }


    }else{
      return null;
    }
  }
  catch (error) {
    throw error;
  }
}



exports.sendMymail = (req, res, next) => {

  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: "email",
      pass: "email account pass"
    }
  });
  //   console.log(transporter);

  transporter.verify(function (error, success) {
    if (error) {
      console.log(error);
    } else {
      console.log("Server is ready to take our messages");
    }
  });
  var message = {
    from: "from email",
    to: "to email",
    subject: "Test Mail from node",
    text: "this is txt DEV",
    // html: "<p>HTML version of the message</p>"
  };
  transporter.sendMail(message, function (err, info) {
    if (err) {
      console.log(err);
    } else {
      console.log(info);
      res.json({ mailres: info })
    }
  })
    ;
}
exports.getDataList = (req, res, next) => {
  let JSONData = this.readList();

  if (Array.isArray(JSONData)) {
    res.json({ status: "success", message: "Data Loaded Successfully", data: JSONData })
  } else { throw new Error("invalid JSON format Stored on server") }

}
exports.saveCred = (req, res, next) => {
  let consts = this.constants;
  let JSONList = [];
  let bodyData = req.body;
  let id = null;

  let { email, password } = bodyData;

  if (email == 'undefined' || password == 'undefined' || email == '' || password == ''|| email == null || password == null) { throw new Error("Insufficient Req params"); }
  path = consts.dir + pth.sep + consts.fileName;

  try {
    if (!fs.existsSync(consts.dir)) {
      fs.mkdirSync(consts.dir);
    }
    if (fs.existsSync(path)) {
      JSONList = this.readList();
      fs.unlinkSync(path);
     
    }
    if (!Array.isArray(JSONList)) {
      JSONList = [];
    }
    let dataLength = JSONList.length;

    id = dataLength + 1;
    JSONList.push({ id: id, "email": email, "password": password });


    
    fs.writeFileSync(path, JSON.stringify(JSONList));
    res.json({ status: "success", message: "Data Stored Successfully" });
  } catch (error) {
    console.log(error);
    next(error);
  }


}
exports.updateCred = (req, res, next) => {
  let consts = this.constants;
  let JSONList = [];
  let bodyData = req.body;
  let { id, email, password } = bodyData;

  if (id == 'undefined' || email == 'undefined' || password == 'undefined', id == '' || email == '' || password == ''||id == null || email == null || password == null) { throw new Error("Insufficient Req params"); }
  path = consts.dir + pth.sep + consts.fileName;

  try {
    if (fs.existsSync(path)) {
      JSONList = this.readList();
      
      if (!Array.isArray(JSONList)) {
        JSONList = [];
      }
      let dataLength = JSONList.length;

      if (dataLength > 0) {
        JSONList = JSONList.map(function (data) {
          if (data.id == id) {
            data = { "id": id, "email": email, "password": password };

          }
          return data;


        })
        fs.unlinkSync(path);
        fs.writeFileSync(path, JSON.stringify(JSONList));
        res.json({ status: "success", message: "Data Updated Successfully" });
      }
    }




  } catch (error) {
    console.log(error);
    next(error);
  }


}
exports.deleteCred = (req, res, next) => {
  let consts = this.constants;
  let JSONList = [];
  
var url_parts = url.parse(req.url, true);
var query = url_parts.query;
  let bodyData = query;
  let { id} = bodyData;

  if (id == "undefined"|| id == '') { throw new Error("Insufficient Req params"); }
  path = consts.dir + pth.sep + consts.fileName;

  try {
    if (fs.existsSync(path)) {
      JSONList = this.readList();
      
      if (!Array.isArray(JSONList)) {
        JSONList = [];
      }
      let dataLength = JSONList.length;

      if (dataLength > 0) {
        console.log(id);
        JSONList = JSONList.filter(function (data) {
          return data.id!=id;
        });
        
        fs.unlinkSync(path);
        fs.writeFileSync(path, JSON.stringify(JSONList));
        res.json({ status: "success", message: "Data Deleted Successfully" });
      }
    }




  } catch (error) {
    console.log(error);
    next(error);
  }


}